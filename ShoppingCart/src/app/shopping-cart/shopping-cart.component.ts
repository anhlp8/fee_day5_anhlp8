import { Component, Injectable, OnInit } from '@angular/core';
import { ProductService } from '../shared/services/product.service';
import { Product } from '../shared/models/product.model';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Cart } from '../shared/models/cart.model';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
@Injectable()
export class ShoppingCartComponent implements OnInit {
  products!: Product[];
  form: FormGroup;
  isCheckoutSuccess = false;
  isCheckoutSubmit = false;

  constructor(private productService: ProductService, private fb: FormBuilder) {
    this.form = fb.group({
      name: ['', Validators.required],
      phone: ['', [Validators.required, Validators.pattern('^0[0-9]{9}')]],
      address: ['', Validators.required],
    })
  }

  ngOnInit(): void {
    this.products = this.productService.getCartProducts();
    this.productService.getCart().subscribe(p => console.log(p));
  }

  getAmount(): number {
    return this.products.reduce((sum, curr) => { return sum + curr.getTotalPrice() }, 0)
  }

  deleteProduct(product: Product) {
    this.productService.deleteCartProduct(product);
  }

  isFormControlValid(control: AbstractControl | null): boolean {
    if (control) {
      return control.valid && (control.touched || control.dirty);
    }
    return false;
  }

  checkoutCart() {
    console.log("checkout cart");

    if (this.form.valid) {
      let cart = new Cart(this.name?.value, this.phone?.value, this.address?.value, this.products);
      this.productService.checkoutCart(cart).subscribe(r => { this.processCheckoutResponse(r); });
    }
  }
  processCheckoutResponse(r: HttpResponse<HttpResponse<Product[]>>) {
    this.isCheckoutSubmit = true;
    console.log(r);
    this.isCheckoutSuccess = r.ok;
  }

  get name() { return this.form.get('name'); }
  get phone() { return this.form.get('phone'); }
  get address() { return this.form.get('address'); }
}