import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from '../shared/models/product.model';
import { ProductService } from '../shared/services/product.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  id!: string;
  product!: Product;
  productForm!: FormGroup;

  constructor(
    private route:ActivatedRoute, 
    private router:Router,
    private fb: FormBuilder,
    private productService: ProductService) {
      this.productForm = this.fb.group({
        productName: ['', Validators.required],
        price: ['', Validators.required],
        quantity: ['', Validators.required]
      });
    }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params =>{
      this.id = params.get('id') ?? '';
      console.log(this.id);
    });

    this.getProductData();
  }

  getProductData() {
    this.productService.getProductById(this.id).subscribe(r =>{
      if(r.ok && r.body != null){
        this.product = r.body;
        this.productName?.setValue(this.product.productName);
        this.price?.setValue(this.product.price);
        this.quantity?.setValue(this.product.quantity);
      }
      else{
        alert('Failed to load data from api');
      }
    })
  }

  submit(){
    this.product.productName = this.productName?.value;
    this.product.price = this.price?.value;
    this.product.quantity = this.quantity?.value;
    this.productService.updateProduct(this.product).subscribe(r => {
      if(r.ok){
        alert('Update successfully');
        this.router.navigateByUrl('/');
      }
      else{
        alert('Update failed')
      }
    });
  }

  get productName() {return this.productForm.get('productName')}
  get price() {return this.productForm.get('price')}
  get quantity() {return this.productForm.get('quantity')}

}
