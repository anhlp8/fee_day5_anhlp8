import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import { filter, map } from 'rxjs/operators';
import {Cart} from '../models/cart.model';
import {Product} from '../models/product.model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  baseUrl = 'https://steelsoftware.azurewebsites.net/api/FresherFPT';
  header = new HttpHeaders({
    'Content-Type': 'application/json',
  });

  private cartProducts: Product[] = [];

  constructor(private client: HttpClient) {}

  getProducts() {
    return this.client.get<Product[]>(this.baseUrl, {headers: this.header});
  }

  addToCart(product: Product): boolean {
    let existProduct = this.cartProducts.find(p => p.id == product.id);
    if (existProduct != null) {
      existProduct.quantity++;
    }
    else {
      let addProduct = new Product(product.id, product.productName, 1, product.price, product.promotionPrice, product.image);
      this.cartProducts.push(addProduct);
    }
    return true;

  }

  getCartProducts() {
    return this.cartProducts;
  }

  getCart(userName: string | undefined = undefined){
    return this.client.get<Cart[]>(this.baseUrl + '/GetShopingCarts')
    .pipe(
      map(c => {
        console.log(c);
        console.log(userName);
        
        let ps: Product[] = [];
        c = (userName == undefined ? c : c.filter(c => c.username == userName))
          .filter(c => c.products != null && c.products.length > 0);
        c.forEach(c => ps = ps.concat(c.products));
        console.log(ps);
        return ps;
      }));
  }

  deleteCartProduct(product: Product) {
    let id = this.cartProducts.indexOf(product);
    if (id >= 0) {
      this.cartProducts.splice(id, 1);
    }
  }

  checkoutCart(cart: Cart) {
    console.log(JSON.stringify(cart));
    return this.client.post<HttpResponse<Product[]>>(
      this.baseUrl + '/CheckOut', 
      cart, 
      {headers: this.header, observe: 'response', responseType: 'json'});
  }

  getProductById(id: string){
    return this.client.get<Product>(
      this.baseUrl + `/${id}`, 
      {headers:this.header, observe: 'response'});
  }

  updateProduct(product: Product){
    return this.client.put(
      this.baseUrl,
      product,
      {headers: this.header, observe: 'response'}
    );
  }
}
