export class Product {
    id: string;
    productName: string;
    quantity: number;
    price: number;
    promotionPrice: number;
    image: string;

    constructor(id: string, productName: string, quantity: number, price: number, promotionPrice: number, image: string) {
        this.id = id;
        this.productName = productName;
        this.quantity = quantity;
        this.price = price;
        this.promotionPrice = promotionPrice;
        this.image = image;
    }

    getTotalPrice() {
        return this.price * this.quantity * (100 - this.promotionPrice) / 100;
    }
}
