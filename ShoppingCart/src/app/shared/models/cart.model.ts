import { Product } from "./product.model";

export class Cart {
    username: string;
    phoneNumber: string;
    address: string;
    products: Product[];

    constructor(
        username: string,
        phoneNumber: string,
        address: string,
        products: Product[],
    ) {
        this.username = username;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.products = products;
    }
}
