import {Component, Injectable, OnInit, Output} from '@angular/core';
import {ProductService} from '../shared/services/product.service';
import {Product} from '../shared/models/product.model';
import {FeedbackMessage} from '../shared/models/feedback-message.model';
import {ToastrService} from 'ngx-toastr';


@Component({
	selector: 'app-products',
	templateUrl: './products.component.html',
	styleUrls: ['./products.component.css']
})
@Injectable()
export class ProductsComponent implements OnInit {
	products!: Product[];
	feedback!: FeedbackMessage | undefined;

	constructor(
		private productService: ProductService,
		private toastr: ToastrService) {
		toastr.toastrConfig.tapToDismiss = true;
		toastr.toastrConfig.positionClass = 'toast-bottom-right';
		toastr.toastrConfig.preventDuplicates = true;
	}

	ngOnInit(): void {
		this.productService.getProducts().subscribe((data: Product[]) => this.products = data);
	}

	addToCart(product: Product) {
		var response = this.productService.addToCart(product);
		if (response) {
			this.toastr.success(`Thêm ${product.productName} vào giỏ hàng!`);
		}
		else {
			this.toastr.error(`Không thể thêm ${product.productName} vào giỏ hàng!`);
		}
	}
}